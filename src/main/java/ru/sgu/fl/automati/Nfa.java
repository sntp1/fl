package ru.sgu.fl.automati;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Nfa {

    public static class Transition {
        private String state;
        private Set<String> input = new HashSet<>();
        private String transition;

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public Set<String> getInput() {
            return input;
        }

        public void setInput(Set<String> input) {
            this.input = input;
        }

        public String getTransition() {
            return transition;
        }

        public void setTransition(String transition) {
            this.transition = transition;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Transition that = (Transition) o;

            if (state != null ? !state.equals(that.state) : that.state != null) return false;
            if (input != null ? !input.equals(that.input) : that.input != null) return false;
            return transition != null ? transition.equals(that.transition) : that.transition == null;

        }

        @Override
        public int hashCode() {
            int result = state != null ? state.hashCode() : 0;
            result = 31 * result + (input != null ? input.hashCode() : 0);
            result = 31 * result + (transition != null ? transition.hashCode() : 0);
            return result;
        }

        public Transition clone() {
            Transition result = new Transition();
            result.setState(getState());
            result.setInput(new HashSet<>(getInput()));
            result.setTransition(getTransition());
            return result;
        }

        @Override
        public String toString() {
            return "Transition{" +
                    "state='" + state + '\'' +
                    ", input=" + input +
                    ", transition='" + transition + '\'' +
                    '}';
        }
    }

    private Set<String> alphabet = new HashSet<>();
    private Set<String> states = new HashSet<>();
    private String startState = "start";
    private Set<String> currentStates = new HashSet<>();
    private Set<String> acceptStates = new HashSet<>();
    private Set<Transition> transitions = new HashSet<>();

    public Nfa() {
    }

    public static Nfa read(String filename) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Nfa nfa = objectMapper.readValue(Nfa.class.getClassLoader().getResourceAsStream(filename), Nfa.class);
            nfa.setCurrentStates(new HashSet<>());
            nfa.getCurrentStates().add(nfa.getStartState());
            return nfa;
        } catch (IOException e) {
            throw new RuntimeException(String.format("Failed to read file %s", filename), e);
        }
    }

    public void reset() {
        getCurrentStates().clear();
        getCurrentStates().add(getStartState());
    }

    public void input(String s) {
        setCurrentStates(getTransition(s));
    }

    public void rawInput(String s) {
        for (int i = 0; i < s.length(); i++) {
            input(s.substring(i, i + 1));
        }
    }

    private Set<String> getTransition(String input) {
        return getTransitions().stream()
                .filter(x -> getCurrentStates().contains(x.getState()) && x.getInput().contains(input))
                .map(Transition::getTransition)
                .collect(Collectors.toSet());
    }

    public boolean isInAcceptState() {
        return getCurrentStates().stream().anyMatch(x -> getAcceptStates().contains(x));
    }

    public boolean isStopped() {
        return getCurrentStates().isEmpty();
    }

    public Nfa clone() {
        Nfa result = new Nfa();
        result.setAlphabet(new HashSet<>(getAlphabet()));
        result.setStates(new HashSet<>(getStates()));
        result.setStartState(getStartState());
        result.setCurrentStates(new HashSet<>(getCurrentStates()));
        result.setAcceptStates(new HashSet<>(getAcceptStates()));
        result.setTransitions(new HashSet<>(getTransitions().stream()
                .map(x -> x.clone())
                .collect(Collectors.toSet())));
        return result;
    }

    public Set<String> getAlphabet() {
        return alphabet;
    }

    public void setAlphabet(Set<String> alphabet) {
        this.alphabet = alphabet;
    }

    public Set<String> getStates() {
        return states;
    }

    public void setStates(Set<String> states) {
        this.states = states;
    }

    public String getStartState() {
        return startState;
    }

    public void setStartState(String startState) {
        this.startState = startState;
    }

    public Set<String> getCurrentStates() {
        return currentStates;
    }

    public void setCurrentStates(Set<String> currentStates) {
        this.currentStates = currentStates;
    }

    public Set<String> getAcceptStates() {
        return acceptStates;
    }

    public void setAcceptStates(Set<String> acceptStates) {
        this.acceptStates = acceptStates;
    }

    public Set<Transition> getTransitions() {
        return transitions;
    }

    public void setTransitions(Set<Transition> transitions) {
        this.transitions = transitions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Nfa nfa = (Nfa) o;

        if (alphabet != null ? !alphabet.equals(nfa.alphabet) : nfa.alphabet != null) return false;
        if (states != null ? !states.equals(nfa.states) : nfa.states != null) return false;
        if (startState != null ? !startState.equals(nfa.startState) : nfa.startState != null) return false;
        if (currentStates != null ? !currentStates.equals(nfa.currentStates) : nfa.currentStates != null) return false;
        if (acceptStates != null ? !acceptStates.equals(nfa.acceptStates) : nfa.acceptStates != null) return false;
        return transitions != null ? transitions.equals(nfa.transitions) : nfa.transitions == null;

    }

    @Override
    public int hashCode() {
        int result = alphabet != null ? alphabet.hashCode() : 0;
        result = 31 * result + (states != null ? states.hashCode() : 0);
        result = 31 * result + (startState != null ? startState.hashCode() : 0);
        result = 31 * result + (currentStates != null ? currentStates.hashCode() : 0);
        result = 31 * result + (acceptStates != null ? acceptStates.hashCode() : 0);
        result = 31 * result + (transitions != null ? transitions.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Nfa{" +
                "alphabet=" + alphabet +
                ", states=" + states +
                ", startState='" + startState + '\'' +
                ", currentStates=" + currentStates +
                ", acceptStates=" + acceptStates +
                ", transitions=" + transitions +
                '}';
    }
}
