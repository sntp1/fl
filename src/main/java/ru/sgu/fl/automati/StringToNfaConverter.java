package ru.sgu.fl.automati;

import ru.sgu.fl.util.NfaUtils;

import java.util.*;

public class StringToNfaConverter {
    private final static String START_STATE = "start";
    private NfaUtils nfaUtils = new NfaUtils();

    public Nfa convert(String s) {
        if (s == null && s.length() == 0) {
            throw new RuntimeException("Invalid argument s");
        }

        List<String> states = new ArrayList<>();
        states.add(START_STATE);
        Set<Nfa.Transition> transitions = new HashSet<>();
        Set<String> alphabet = new HashSet<>();

        for (char c : s.toCharArray()) {
            String input = String.valueOf(c);
            alphabet.add(input);
            String state = nfaUtils.generateUniqueName(input, states);

            Nfa.Transition transition = new Nfa.Transition();
            Set<String> inputs = new HashSet<>();
            inputs.add(input);
            transition.setState(states.get(states.size() - 1));
            transition.setInput(inputs);
            transition.setTransition(state);
            transitions.add(transition);
            states.add(state);
        }

        Set<String> acceptStates = new HashSet<>();
        acceptStates.add(states.get(states.size() - 1));

        Set<String> currentStates = new HashSet<>();
        currentStates.add(START_STATE);

        Nfa nfa = new Nfa();
        nfa.setAlphabet(alphabet);
        nfa.setStates(new HashSet<>(states));
        nfa.setTransitions(transitions);
        nfa.setStartState(states.get((0)));
        nfa.setCurrentStates(currentStates);
        nfa.setAcceptStates(acceptStates);

        return nfa;
    }
}
