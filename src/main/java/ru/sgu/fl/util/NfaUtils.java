package ru.sgu.fl.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import ru.sgu.fl.automati.Nfa;
import ru.sgu.fl.util.FlCollections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class NfaUtils {
    private static final Pattern DIGIT_IN_THE_END_PATTERN = Pattern.compile("\\d+$");

    public Nfa union(Nfa a, Nfa b) {
        Nfa u = a.clone();
        u.setAlphabet(FlCollections.union(u.getAlphabet(), b.getAlphabet()));
        rename(u, u.getStartState(), b.getStartState());

        Set<String> statesToRename = FlCollections.interception(a.getStates(), b.getStates());
        statesToRename.remove(a.getStartState());
        statesToRename.remove(b.getStartState());

        for (String stateToRename : statesToRename) {
            Set<String> allStates = FlCollections.union(u.getStates(), b.getStates());
            renameState(u, stateToRename, allStates);
        }

        u.getStates().addAll(b.getStates());
        u.getAcceptStates().addAll(b.getAcceptStates());
        u.getTransitions().addAll(b.getTransitions().stream()
                .map(x -> x.clone())
                .collect(Collectors.toSet()));
        return u;
    }

    public Nfa iteration(Nfa a) {
        Nfa u = a.clone();

        Set<String> startStates = extractStartStates(u);
        for (String acceptState : u.getAcceptStates()) {
            for (String startState : startStates) {
                Nfa.Transition transition = new Nfa.Transition();
                transition.setState(acceptState);
                transition.setTransition(startState);
                Set<String> input = u.getTransitions().stream()
                        .filter(x -> x.getState().equals(u.getStartState()) && x.getTransition().equals(startState))
                        .map(x -> x.getInput())
                        .findFirst()
                        .get();
                transition.setInput(input);
                u.getTransitions().add(transition);
            }
        }
        u.getAcceptStates().add(u.getStartState());

        return u;
    }

    public Nfa concat(Nfa a, Nfa b) {
        Nfa c = a.clone();

        c.setAlphabet(FlCollections.union(c.getAlphabet(), b.getAlphabet()));

        Set<String> statesToRename = FlCollections.interception(a.getStates(), b.getStates());
        statesToRename.remove(a.getStartState());
        statesToRename.remove(b.getStartState());

        for (String stateToRename : statesToRename) {
            Set<String> allStates = FlCollections.union(c.getStates(), b.getStates());
            renameState(c, stateToRename, allStates);
        }

        c.getStates().addAll(b.getStates());

        c.getTransitions().addAll(b.getTransitions().stream()
                .filter(x -> !x.getState().equals(b.getStartState()))
                .collect(Collectors.toSet()));

        Set<String> bStartStates = extractStartStates(b);
        for (String acceptState : c.getAcceptStates()) {
            for (String startState : bStartStates) {
                Nfa.Transition transition = new Nfa.Transition();
                transition.setState(acceptState);
                transition.setTransition(startState);
                Set<String> input = b.getTransitions().stream()
                        .filter(x -> x.getState().equals(b.getStartState()) && x.getTransition().equals(startState))
                        .map(x -> x.getInput())
                        .findFirst()
                        .get();
                transition.setInput(input);
                c.getTransitions().add(transition);
            }
        }

        if (!b.getAcceptStates().contains(b.getStartState())) {
            c.getAcceptStates().clear();
        }

        c.getAcceptStates().addAll(new HashSet<>(b.getAcceptStates()));
        c.getAcceptStates().remove(b.getStartState());

        return c;
    }

    public Nfa oneOrNone(Nfa a) {
        Nfa o = a.clone();
        o.getAcceptStates().add(o.getStartState());
        return o;
    }

    public Nfa oneOrMore(Nfa a) {
        Nfa oneOrMore = iteration(a);
        oneOrMore.getAcceptStates().remove(oneOrMore.getStartState());
        return oneOrMore;
    }

    private Set<String> extractStartStates(Nfa a) {
        return a.getTransitions().stream()
                .filter(x -> x.getState().equals(a.getStartState()))
                .map(x -> x.getTransition())
                .collect(Collectors.toSet());
    }


    private void renameState(Nfa nfa, String stateToRename, Collection<String> occupiedNames) {
        String renamedState = generateUniqueName(stateToRename, occupiedNames);
        rename(nfa, stateToRename, renamedState);
    }

    private void rename(Nfa nfa, String oldName, String newName) {
        FlCollections.replace(nfa.getStates(), oldName, newName);
        FlCollections.replace(nfa.getAcceptStates(), oldName, newName);
        FlCollections.replace(nfa.getCurrentStates(), oldName, newName);
        if (oldName.equals(nfa.getStartState())) {
            nfa.setStartState(newName);
        }
        nfa.getTransitions().stream()
                .filter(x -> x.getState().equals(oldName))
                .forEach(x -> x.setState(newName));

        nfa.getTransitions().stream()
                .filter(x -> x.getTransition().equals(oldName))
                .forEach(x -> x.setTransition(newName));
    }

    public String generateUniqueName(String name, Collection<String> occupiedNames) {
        if (!occupiedNames.contains(name)) {
            return name;
        }

        Matcher matcher = DIGIT_IN_THE_END_PATTERN.matcher(name);
        int number = 1;
        String originalName = name;
        if (matcher.find()) {
            number = Integer.parseInt(matcher.group()) + 1;
            originalName = name.substring(0, matcher.start());
        }

        String result = originalName + String.valueOf(number);
        while (occupiedNames.contains(result)) {
            number += 1;
            result = originalName + String.valueOf(number);
        }
        return result;
    }
}
