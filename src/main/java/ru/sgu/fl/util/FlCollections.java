package ru.sgu.fl.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class FlCollections {

    public static <T> Set<T> union(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<>(a);
        result.addAll(b);
        return result;
    }

    public static <T> Set<T> interception(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<>(a);
        result.retainAll(b);
        return result;
    }

    public static <T> Set<T> difference(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<>(a);
        result.removeAll(b);
        return result;
    }

    public static <T> void replace(Collection<T> collection, T oldElem, T newElem) {
        if (collection.contains(oldElem)) {
            collection.remove(oldElem);
            collection.add(newElem);
        }
    }
}
