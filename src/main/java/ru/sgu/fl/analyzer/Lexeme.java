package ru.sgu.fl.analyzer;

public class Lexeme {
    private String className;
    private String token;

    public Lexeme(String className, String token) {
        this.className = className;
        this.token = token;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lexeme lexeme = (Lexeme) o;

        if (className != null ? !className.equals(lexeme.className) : lexeme.className != null) return false;
        return token != null ? token.equals(lexeme.token) : lexeme.token == null;

    }

    @Override
    public int hashCode() {
        int result = className != null ? className.hashCode() : 0;
        result = 31 * result + (token != null ? token.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Lexeme{" +
                "className='" + className + '\'' +
                ", token='" + token.replace("\n", "\\n").replace("\r", "\\r").replace("\t", "\\t") + '\'' +
                '}';
    }
}
