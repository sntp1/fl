package ru.sgu.fl.analyzer;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.sgu.fl.analyzer.regex.RegExParser;
import ru.sgu.fl.automati.Nfa;
import ru.sgu.fl.automati.StringToNfaConverter;
import ru.sgu.fl.util.NfaUtils;

import java.io.IOException;
import java.util.Arrays;

public class LexicalAnalyzerFactory {
    private NfaUtils nfaUtils = new NfaUtils();
    private StringToNfaConverter stringToNfaConverter = new StringToNfaConverter();
    private RegExParser regExParser = new RegExParser(this);

    public LexicalAnalyzer getLexicalAnalyzerForTask3() {
        LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer();
        String[] keywords = {"var", "fun", "in", "let", "end", "if", "then", "else", "val"};
        String[] logical = {"true", "false"};
        String[] operations = {"+", "-", "*", "/", "andalso", "orelse", "not"};

        Nfa kw = uniteSimpleAutomates(keywords);
        Nfa log = uniteSimpleAutomates(logical);
        Nfa op = uniteSimpleAutomates(operations);
        Nfa num = Nfa.read("double.json");
        Nfa integer = Nfa.read("int.json");
        Nfa as = stringToNfaConverter.convert("=");
        Nfa lb = stringToNfaConverter.convert("(");
        Nfa rb = stringToNfaConverter.convert(")");
        Nfa ws = Nfa.read("whitespace.json");
        Nfa id = Nfa.read("id.json");
        Nfa c = stringToNfaConverter.convert(",");

        lexicalAnalyzer.getAutomatas().put(Constants.KEYWORD, kw);
        lexicalAnalyzer.getAutomatas().put(Constants.INTEGER, integer);
        lexicalAnalyzer.getAutomatas().put(Constants.NUMBER, num);

        lexicalAnalyzer.getAutomatas().put(Constants.LOGICAL, log);
        lexicalAnalyzer.getAutomatas().put(Constants.OPERATION, op);
        lexicalAnalyzer.getAutomatas().put(Constants.ASSIGNMENT, as);
        lexicalAnalyzer.getAutomatas().put(Constants.LEFT_BRACE, lb);
        lexicalAnalyzer.getAutomatas().put(Constants.RIGHT_BRACE, rb);
        lexicalAnalyzer.getAutomatas().put(Constants.WHITESPACE, ws);
        lexicalAnalyzer.getAutomatas().put(Constants.COMMA, c);

        lexicalAnalyzer.getAutomatas().put(Constants.COMMA, id);
        return lexicalAnalyzer;
    }

    private Nfa uniteSimpleAutomates(String[] strings) {
        return Arrays.stream(strings)
                .map(stringToNfaConverter::convert)
                .reduce(new Nfa(), nfaUtils::union);
    }

    public LexicalAnalyzer getRegExAnalyzer() {
        LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer();
        String bracketSymbols[] = new String[] {"(", ")"};
        String binaryOperationSymbols[] = new String[] {"|"};
        String postFixOperationSymbols[] = new String[] {"*", "+", "?"};
        String specialSymbols[] = new String[] {"\\d", "\\w"};
        String escapedSymbols[] = new String[] {"\\\\", "\\(", "\\)", "\\|", "\\*", "\\+", "\\?"};

        Nfa brackets = uniteSimpleAutomates(bracketSymbols);
        Nfa binaryOperations = uniteSimpleAutomates(binaryOperationSymbols);
        Nfa postFixOperations = uniteSimpleAutomates(postFixOperationSymbols);
        Nfa specials = uniteSimpleAutomates(specialSymbols);
        Nfa escaped = uniteSimpleAutomates(escapedSymbols);
        Nfa string = Nfa.read("string.json");

        lexicalAnalyzer.getAutomatas().put(Constants.BRACE, brackets);
        lexicalAnalyzer.getAutomatas().put(Constants.BINARY_OPERATION, binaryOperations);
        lexicalAnalyzer.getAutomatas().put(Constants.POSTFIX_OPERATION, postFixOperations);
        lexicalAnalyzer.getAutomatas().put(Constants.SPECIAL, specials);
        lexicalAnalyzer.getAutomatas().put(Constants.ESCAPED, escaped);
        lexicalAnalyzer.getAutomatas().put(Constants.STRING, string);

        return lexicalAnalyzer;
    }

    public LexicalAnalyzer readFromFile(String filename) {
        ObjectMapper objectMapper = new ObjectMapper();
        LexicalAnalyzerInput input;
        try {
            input = objectMapper.readValue(Nfa.class.getClassLoader().getResourceAsStream(filename), LexicalAnalyzerInput.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        for (LexicalAnalyzerInput.InputItem item : input.getClasses()) {
            Nfa nfa = regExParser.parse(item.getRegExp());
            analyzer.getAutomatas().put(item.getClassName(), nfa);
        }

        return analyzer;
    }
}
