package ru.sgu.fl.analyzer;

import ru.sgu.fl.automati.Nfa;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class LexicalAnalyzer {

    private class ParsingResult {
        private boolean recognized;
        private int length;

        public ParsingResult(boolean recognized, int length) {
            this.recognized = recognized;
            this.length = length;
        }

        public boolean isRecognized() {
            return recognized;
        }

        public void setRecognized(boolean recognized) {
            this.recognized = recognized;
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }
    }

    private LinkedHashMap<String, Nfa> automatas = new LinkedHashMap<>();

    public LexicalAnalyzer() {
    }

    public LexicalAnalyzer(LinkedHashMap<String, Nfa> automatas) {
        this.automatas = automatas;
    }

    public List<Lexeme> analyze(String text) {
        List<Lexeme> result = new ArrayList<>();
        int i = 0;
        while (i < text.length()) {
            int max = 0;
            String className = null;
            for (Map.Entry<String, Nfa> entry : automatas.entrySet()) {
                ParsingResult parsingResult = parse(entry.getValue(), text, i);
                if (parsingResult.isRecognized() && parsingResult.getLength() > max) {
                    max = parsingResult.getLength();
                    className = entry.getKey();
                }
            }
            if (className != null) {
                result.add(new Lexeme(className, text.substring(i, i + max)));
            } else {
                throw new RuntimeException(String.format("Failed to recognize symbol on position %d", i));
            }
            i += max;
        }
        return result;
    }

    private ParsingResult parse(Nfa nfa, String text, int start) {
        nfa.reset();
        int m = 0;
        boolean recognized = false;
        for (char c : text.substring(start).toCharArray()) {
            String input = String.valueOf(c);
            nfa.input(input);
            if (nfa.isStopped()) {
                break;
            }
            recognized = true;
            m++;
        }
        return new ParsingResult(recognized, m);
    }

    public LinkedHashMap<String, Nfa> getAutomatas() {
        return automatas;
    }

    public void setAutomatas(LinkedHashMap<String, Nfa> automatas) {
        this.automatas = automatas;
    }
}
