package ru.sgu.fl.analyzer;

import java.util.List;

public class LexicalAnalyzerInput {
    public static class InputItem {
        private String className;
        private String regExp;

        public InputItem(String className, String regExp) {
            this.className = className;
            this.regExp = regExp;
        }

        public InputItem() {
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getRegExp() {
            return regExp;
        }

        public void setRegExp(String regExp) {
            this.regExp = regExp;
        }
    }

    private List<InputItem> classes;

    public List<InputItem> getClasses() {
        return classes;
    }

    public void setClasses(List<InputItem> classes) {
        this.classes = classes;
    }
}
