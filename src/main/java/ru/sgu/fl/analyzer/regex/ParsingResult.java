package ru.sgu.fl.analyzer.regex;

import ru.sgu.fl.analyzer.Lexeme;
import ru.sgu.fl.automati.Nfa;

public class ParsingResult {
    private int startPosition;
    private int endPosition;
    private Nfa nfa;
    private Lexeme lexeme;

    public ParsingResult(int startPosition, int endPosition, Nfa nfa, Lexeme lexeme) {
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.nfa = nfa;
        this.lexeme = lexeme;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    public int getEndPosition() {
        return endPosition;
    }

    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }

    public Nfa getNfa() {
        return nfa;
    }

    public void setNfa(Nfa nfa) {
        this.nfa = nfa;
    }

    public Lexeme getLexeme() {
        return lexeme;
    }

    public void setLexeme(Lexeme lexeme) {
        this.lexeme = lexeme;
    }

    @Override
    public String toString() {
        return "ParsingResult{" +
                "startPosition=" + startPosition +
                ", endPosition=" + endPosition +
                ", nfa=" + nfa +
                ", lexeme=" + lexeme +
                '}';
    }
}
