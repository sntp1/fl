package ru.sgu.fl.analyzer.regex;

import ru.sgu.fl.analyzer.Constants;
import ru.sgu.fl.analyzer.Lexeme;
import ru.sgu.fl.analyzer.LexicalAnalyzer;
import ru.sgu.fl.analyzer.LexicalAnalyzerFactory;
import ru.sgu.fl.automati.Nfa;
import ru.sgu.fl.automati.StringToNfaConverter;
import ru.sgu.fl.util.NfaUtils;

import java.util.*;

public class RegExParser {
    private StringToNfaConverter stringToNfaConverter = new StringToNfaConverter();
    private NfaUtils nfaUtils = new NfaUtils();
    private LexicalAnalyzerFactory factory;
    private LexicalAnalyzer analyzer;
    private Map<Integer, ParsingResult> results;

    private final int NO_OCCURANCE = -1;

    public RegExParser(LexicalAnalyzerFactory factory) {
        this.factory = factory;
        this.analyzer = factory.getRegExAnalyzer();
    }

    public Nfa parse(String regex) {
        List<Lexeme> lexemes = analyzer.analyze(regex);
        results = new HashMap<>();
        return parse(lexemes, 0, lexemes.size() - 1);
    }

    private Nfa parse(List<Lexeme> lexemes, int start, int end) {
        Stack<Integer> openningBracketPositions = new Stack<>();
        List<Integer> topSubExPositions = new ArrayList<>();

        for (int i = start; i <= end; i++) {
            String token = lexemes.get(i).getToken();
            if ("(".equals(token)) {
                if (openningBracketPositions.empty()) {
                    topSubExPositions.add(i + 1);
                }
                openningBracketPositions.push(i);
            }
            if (")".equals(token)) {
                int subExStart = openningBracketPositions.pop() + 1;
                int subExEnd = i - 1;
                ParsingResult parsingResult = lookUpResult(subExStart);
                if (parsingResult == null) {
                    Nfa subExpNfa = parse(lexemes, subExStart, subExEnd);
                    results.put(subExStart, new ParsingResult(subExStart, subExEnd, subExpNfa, null));
                }
            }
        }

        List<ParsingResult> parsingResults = new ArrayList<>();
        for (int i = start; i <= end; i++) {
            ParsingResult parsingResult = lookUpResult(i);
            if (parsingResult != null) {
                parsingResults.add(parsingResult);
                i = parsingResult.getEndPosition();
            } else if (!Constants.BRACE.equals(lexemes.get(i).getClassName())) {
                parsingResults.add(new ParsingResult(i, i + 1, null, lexemes.get(i)));
            }
        }

        // replace strings, escapes and specials
        for (int i = 0; i < parsingResults.size(); i++) {
            parsingResults.set(i, process(parsingResults.get(i)));
        }

        // apply postfix operations
        for (int i = 0; i < parsingResults.size(); i++) {
            ParsingResult parsingResult = parsingResults.get(i);
            if (parsingResult.getLexeme() != null
                    && Constants.POSTFIX_OPERATION.equals(parsingResult.getLexeme().getClassName())) {
                if (Constants.ITER.equals(parsingResult.getLexeme().getToken())) {
                    Nfa iter = nfaUtils.iteration(parsingResults.get(i-1).getNfa());
                    parsingResults.get(i - 1).setNfa(iter);
                    parsingResults.get(i - 1).setEndPosition(parsingResult.getEndPosition());
                    parsingResults.remove(i);
                    i--;
                } else if (Constants.QUESTION.equals(parsingResult.getLexeme().getToken())) {
                    Nfa presentOrNot = nfaUtils.oneOrNone(parsingResults.get(i-1).getNfa());
                    parsingResults.get(i - 1).setNfa(presentOrNot);
                    parsingResults.get(i - 1).setEndPosition(parsingResult.getEndPosition());
                    parsingResults.remove(i);
                    i--;
                } else if (Constants.PLUS.equals(parsingResult.getLexeme().getToken())) {
                    Nfa oneOrMore = nfaUtils.oneOrMore(parsingResults.get(i-1).getNfa());
                    parsingResults.get(i - 1).setNfa(oneOrMore);
                    parsingResults.get(i - 1).setEndPosition(parsingResult.getEndPosition());
                    parsingResults.remove(i);
                    i--;
                }
            }
        }

        // apply binary operations
        for (int i = 1; i < parsingResults.size() - 1; i++) {
            ParsingResult parsingResult = parsingResults.get(i);
            if (parsingResult.getLexeme() != null
                    && Constants.BINARY_OPERATION.equals(parsingResult.getLexeme().getClassName())) {
                if (Constants.UNION.equals(parsingResult.getLexeme().getToken())) {
                    Nfa union = nfaUtils.union(parsingResults.get(i - 1).getNfa(), parsingResults.get(i + 1).getNfa());
                    parsingResults.get(i - 1).setNfa(union);
                    parsingResults.get(i - 1).setEndPosition(parsingResults.get(i+1).getEndPosition());
                    parsingResults.remove(i + 1);
                    parsingResults.remove(i);
                    i--;
                }
            }
        }

        // parsingResults.forEach(System.out::println);
        // System.out.println();

        if (parsingResults.size() > 1) {
            return parsingResults.subList(1, parsingResults.size()).stream()
                    .map(x -> x.getNfa())
                    .reduce(parsingResults.get(0).getNfa(), nfaUtils::concat);
        } else {
            return parsingResults.get(0).getNfa();
        }
    }

    private ParsingResult lookUpResult(int start) {
        return results.containsKey(start)? results.get(start) : null;
    }

    private ParsingResult process(ParsingResult parsingResult) {
        if (parsingResult.getNfa() != null) {
            return parsingResult;
        }
        String className = parsingResult.getLexeme().getClassName();
        String token = parsingResult.getLexeme().getToken();
        ParsingResult newParsingResult = new ParsingResult(parsingResult.getStartPosition(),
                parsingResult.getEndPosition(),
                null,
                null);
        if (Constants.STRING.equals(className)) {
            newParsingResult.setNfa(stringToNfaConverter.convert(token));
            return newParsingResult;
        } else if (Constants.SPECIAL.equals(className)) {
            if (Constants.D.equals(token)) {
                newParsingResult.setNfa(Nfa.read("digits.json"));
            } else if (Constants.W.equals(token)) {
                newParsingResult.setNfa(Nfa.read("w.json"));
            }
            return newParsingResult;
        } else if (Constants.ESCAPED.equals(className)) {
            Nfa nfa = stringToNfaConverter.convert(token.replaceFirst("\\\\", ""));
            newParsingResult.setNfa(nfa);
            return newParsingResult;
        }

        return parsingResult;
    }

}
