package ru.sgu.fl.analyzer;

public class Constants {

    public static final String KEYWORD = "keyword";
    public static final String INTEGER = "integer";
    public static final String NUMBER = "number";
    public static final String LOGICAL = "logical";
    public static final String OPERATION = "operation";
    public static final String ASSIGNMENT = "assignment";
    public static final String LEFT_BRACE = "left brace";
    public static final String RIGHT_BRACE = "right brace";
    public static final String WHITESPACE = "whitespace";
    public static final String COMMA = "comma";
    public static final String ID = "id";


    public static final String BRACE = "brace";
    public static final String BINARY_OPERATION = "binary operation";
    public static final String POSTFIX_OPERATION = "postfix operation";
    public static final String SPECIAL = "special";
    public static final String ESCAPED = "escaped";
    public static final String STRING = "string";

    public static final String D = "\\d";
    public static final String W = "\\w";

    public static final String ITER = "*";
    public static final String UNION = "|";
    public static final String PLUS = "+";
    public static final String QUESTION = "?";
}
