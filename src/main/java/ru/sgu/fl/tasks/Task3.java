package ru.sgu.fl.tasks;

import org.apache.commons.io.FileUtils;
import ru.sgu.fl.analyzer.Lexeme;
import ru.sgu.fl.analyzer.LexicalAnalyzer;
import ru.sgu.fl.analyzer.LexicalAnalyzerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Task3 {
    private LexicalAnalyzerFactory factory = new LexicalAnalyzerFactory();
    private LexicalAnalyzer lexicalAnalyzer = factory.getLexicalAnalyzerForTask3();

    public static void main(String[] args) throws IOException {
        Task3 task3 = new Task3();
        String text = FileUtils.readFileToString(
                new File(Task3.class.getClassLoader().getResource("task3.txt").getFile()),
                "utf-8");
        List<Lexeme> lexemes = task3.lexicalAnalyzer.analyze(text);
        for (Lexeme lexeme : lexemes) {
            System.out.println(lexeme);
        }
    }
}
