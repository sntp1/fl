package ru.sgu.fl.tasks;

import ru.sgu.fl.automati.Nfa;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Task1 {
    private Nfa nfa = Nfa.read("double.json");

    public static void main(String[] args) {
        Task1 task = new Task1();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                task.nfa.reset();
                String raw = br.readLine();
                task.nfa.rawInput(raw);
                if (task.nfa.isInAcceptState()) {
                    System.out.println("Correct!");
                } else {
                    System.out.println("Incorrect!");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
