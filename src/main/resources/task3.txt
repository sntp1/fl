var PI = 3.14

fun pow(x)
	x*x

fun square(radius)
    PI * pow(radius)

square(5)