package ru.sgu.fl.automati;


import junit.framework.TestCase;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.sgu.fl.analyzer.regex.RegExParser;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class DoubleTest extends TestCase {
    private Nfa nfa = Nfa.read("double.json");

    @Before
    public void setUp() {
        nfa.reset();
    }

    @Test
    public void testFull() {
        nfa.rawInput("+123.345e123");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testFullAndUppercasedE() {
        nfa.rawInput("123.456E+123");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testStartsWithPoint() {
        nfa.rawInput(".123e-1");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testNegative1() {
        nfa.rawInput("-1");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testNegativeFraction() {
        nfa.rawInput("-.1");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testFraction() {
        nfa.rawInput(".1");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testIntegerWithPoint() {
        nfa.rawInput("1.");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testPositive() {
        nfa.rawInput("+1");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testPoint() {
        nfa.rawInput(".");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testPositivePoint() {
        nfa.rawInput("+.");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testChars() {
        nfa.rawInput("fsahsdf");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testWithoutFraction() {
        nfa.rawInput("+123.e3");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testMultipleSigns() {
        nfa.rawInput("+-1.2e2");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testMultiplePoints() {
        nfa.rawInput("+1..2e2");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testPointAndE() {
        nfa.rawInput(".e1");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testSign() {
        nfa.rawInput("+");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testWithoutNumber() {
        nfa.rawInput("e123");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testMultipleSecondSign() {
        nfa.rawInput("123.34E-+1");
        assertFalse(nfa.isInAcceptState());
    }

}
