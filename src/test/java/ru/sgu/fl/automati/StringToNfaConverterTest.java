package ru.sgu.fl.automati;

import org.junit.Test;

public class StringToNfaConverterTest {
    private StringToNfaConverter stringToNfaConverter = new StringToNfaConverter();

    @Test
    public void testVar() {
        Nfa nfa = stringToNfaConverter.convert("var");
        System.out.println(nfa);
    }

    @Test
    public void testElse() {
        Nfa nfa = stringToNfaConverter.convert("else");
        System.out.println(nfa);
    }
}
