package ru.sgu.fl.automati;


import junit.framework.TestCase;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class ZeroTest {
    private Nfa nfa = Nfa.read("0.json");

    @Before
    public void setUp() {
        nfa.reset();
    }

    @Test
    public void testEndsWithZero() {
        nfa.rawInput("1010");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testEndsWithOne() {
        nfa.rawInput("101");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testEmpty() {
        nfa.rawInput("");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testEndsWithChars() {
        nfa.rawInput("00101010wd");
        assertFalse(nfa.isInAcceptState());
    }
}
