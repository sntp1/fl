package ru.sgu.fl.automati;

import junit.framework.TestCase;
import org.junit.Test;
import ru.sgu.fl.util.NfaUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

/**
 * Created by sntp on 11.10.16.
 */
public class NfaUtilsTest extends TestCase {
    NfaUtils nfaUtils = new NfaUtils();
    StringToNfaConverter stringToNfaConverter = new StringToNfaConverter();

    @Test
    public void testGenerateUniqueName_DoubleIncrement() {
        String name = "name8";
        List<String> occupiedNames = Arrays.asList(new String[]{"name8", "name9"});
        assertEquals("name10", nfaUtils.generateUniqueName(name, occupiedNames));
    }

    @Test
    public void testGenerateUniqueName_NameIsNotOccupied() {
        String name = "name";
        List<String> occupiedNames = Arrays.asList(new String[]{"name1"});
        assertEquals("name", nfaUtils.generateUniqueName(name, occupiedNames));
    }

    @Test
    public void testGenerateUniqueName_NameIsOccupied() {
        String name = "name";
        List<String> occupiedNames = Arrays.asList(new String[]{"name"});
        assertEquals("name1", nfaUtils.generateUniqueName(name, occupiedNames));
    }

    @Test
    public void testUnion() {
        Nfa nfa1 = Nfa.read("test/union-test1.json");
        Nfa nfa2 = Nfa.read("test/union-test2.json");
        Nfa union = nfaUtils.union(nfa1, nfa2);
        System.out.println(union);
        System.out.println(nfa1);
        System.out.println(nfa2);
    }

    @Test
    public void testConcat() {
        Nfa a = stringToNfaConverter.convert("a");
        Nfa b = stringToNfaConverter.convert("b");

        Nfa c = nfaUtils.concat(a, b);
        System.out.println(c);

        c.rawInput("ab");
        assertTrue(c.isInAcceptState());
    }

    @Test
    public void testConcat2() {
        Nfa a = stringToNfaConverter.convert("a");
        Nfa b = stringToNfaConverter.convert("b");

        Nfa c = nfaUtils.concat(a, b);
        System.out.println(c);

        c.rawInput("b");
        assertFalse(c.isInAcceptState());
    }

    @Test
    public void testOneOrMore() {
        Nfa a = stringToNfaConverter.convert("a");
        Nfa oneOrmore = nfaUtils.oneOrMore(a);
        oneOrmore.rawInput("aaa");
        assertTrue(oneOrmore.isInAcceptState());
    }

    @Test
    public void testOneOrMore2() {
        Nfa a = stringToNfaConverter.convert("a");
        Nfa oneOrmore = nfaUtils.oneOrMore(a);
        System.out.println(oneOrmore);
        oneOrmore.rawInput("a");
        assertTrue(oneOrmore.isInAcceptState());
    }

    @Test
    public void testOneOrNone() {
        Nfa a = stringToNfaConverter.convert("a");
        Nfa oneOrNone = nfaUtils.oneOrNone(a);
        System.out.println(oneOrNone);

        oneOrNone.rawInput("a");
        assertTrue(oneOrNone.isInAcceptState());
    }

    @Test
    public void testOneOrNone2() {
        Nfa a = stringToNfaConverter.convert("a");
        Nfa oneOrNone = nfaUtils.oneOrNone(a);
        System.out.println(oneOrNone);

        oneOrNone.rawInput("");
        assertTrue(oneOrNone.isInAcceptState());
    }

    @Test
    public void testOneOrNone3() {
        Nfa a = stringToNfaConverter.convert("a");
        Nfa oneOrNone = nfaUtils.oneOrNone(a);
        System.out.println(oneOrNone);

        oneOrNone.rawInput("aa");
        assertFalse(oneOrNone.isInAcceptState());
    }

    @Test
    public void testConcatAndIter() {
        Nfa a = stringToNfaConverter.convert("a");
        Nfa b = stringToNfaConverter.convert("b");
        Nfa bInter = nfaUtils.iteration(b);
        Nfa c = nfaUtils.concat(a, bInter);

        System.out.println(c);
        c.rawInput("a");
        assertTrue(c.isInAcceptState());
    }
}
