package ru.sgu.analyzer.regex;


import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

import ru.sgu.fl.analyzer.LexicalAnalyzerFactory;
import ru.sgu.fl.analyzer.regex.RegExParser;
import ru.sgu.fl.automati.Nfa;

public class RegExParserTest extends TestCase{
    RegExParser parser = new RegExParser(new LexicalAnalyzerFactory());

    @Test
    public void testString() {
        Nfa nfa = parser.parse("else");
        System.out.println(nfa);
        nfa.rawInput("else");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testIter() {
        Nfa nfa = parser.parse("a*");
        System.out.println(nfa);
        nfa.rawInput("aaa");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testIter2() {
        Nfa nfa = parser.parse("a*b");
        System.out.println(nfa);
        nfa.rawInput("aab");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testUnion() {
        Nfa nfa = parser.parse("a|b");
        System.out.println(nfa);
        nfa.rawInput("b");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testBrackets() {
        Nfa nfa = parser.parse("(a|b)c");
        System.out.println(nfa);
        nfa.rawInput("ac");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testDoubleBrackets() {
        Nfa nfa = parser.parse("(a|b)|(c|d)");
        System.out.println(nfa);
        nfa.rawInput("d");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testInnerBrackets() {
        Nfa nfa = parser.parse("(a|(b|c))");
        System.out.println(nfa);
        nfa.rawInput("b");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testD() {
        Nfa nfa = parser.parse("\\d");
        System.out.println(nfa);
        nfa.rawInput("12345");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testW() {
        Nfa nfa = parser.parse("\\w");
        System.out.println(nfa);
        nfa.rawInput("HelloWorld");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testEscape() {
        Nfa nfa = parser.parse("\\(a\\*\\|b\\)");
        System.out.println(nfa);
        nfa.rawInput("(a*|b)");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testFull() {
        Nfa nfa = parser.parse("(\\d*|(\\+|-)).");
        System.out.println(nfa);
        nfa.rawInput("1.");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testBraceHell() {
        Nfa nfa = parser.parse("((((a))))");
        System.out.println(nfa);
        nfa.rawInput("a");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testOneOrMore() {
        Nfa nfa = parser.parse("a+");
        System.out.println(nfa);
        nfa.rawInput("aaa");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testOneOrMore1() {
        Nfa nfa = parser.parse("a+");
        System.out.println(nfa);
        nfa.rawInput("");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testOneOrMore2() {
        Nfa nfa = parser.parse("a+");
        System.out.println(nfa);
        nfa.rawInput("a");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testOneOrNone() {
        Nfa nfa = parser.parse("a?");
        System.out.println(nfa);
        nfa.rawInput("a");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testOneOrNone2() {
        Nfa nfa = parser.parse("a?");
        System.out.println(nfa);
        nfa.rawInput("");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testOneOrNone3() {
        Nfa nfa = parser.parse("a?");
        System.out.println(nfa);
        nfa.rawInput("aa");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testInteger() {
        Nfa nfa = parser.parse("(\\+|-)?\\d+");
        System.out.println(nfa);
        nfa.rawInput("123");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testInteger2() {
        Nfa nfa = parser.parse("(\\+|-)?\\d+");
        System.out.println(nfa);
        nfa.rawInput("+123");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testInteger3() {
        Nfa nfa = parser.parse("(\\+|-)?\\d+");
        System.out.println(nfa);
        nfa.rawInput("-");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testInteger4() {
        Nfa nfa = parser.parse("(\\+|-)?\\d+");
        System.out.println(nfa);
        nfa.rawInput("+-2");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testDouble() {
        Nfa nfa = parser.parse("(\\+|-)?\\d*.?\\d+((e|E)(\\+|-)?\\d+)?");
        System.out.println(nfa);

        nfa.rawInput("e123");
        assertFalse(nfa.isInAcceptState());
    }

    @Test
    public void testDouble2() {
        Nfa nfa = parser.parse("(\\+|-)?\\d*");
        System.out.println(nfa);

        nfa.rawInput("-42");
        assertTrue(nfa.isInAcceptState());
    }

    @Test
    public void testArsen() {
        Nfa nfa = parser.parse("begi(n|e)nd");
        System.out.println(nfa);

        nfa.rawInput("begiend");
        assertTrue(nfa.isInAcceptState());
    }

}
