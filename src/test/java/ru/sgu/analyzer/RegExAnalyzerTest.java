package ru.sgu.analyzer;


import org.junit.Test;
import ru.sgu.fl.analyzer.Lexeme;
import ru.sgu.fl.analyzer.LexicalAnalyzer;
import ru.sgu.fl.analyzer.LexicalAnalyzerFactory;
import ru.sgu.fl.automati.Nfa;

import java.util.List;

public class RegExAnalyzerTest {
    private LexicalAnalyzerFactory factory = new LexicalAnalyzerFactory();

    @Test
    public void test() {
        LexicalAnalyzer analyzer = factory.getRegExAnalyzer();
        List<Lexeme> lexemes = analyzer.analyze("(a|b)*\\d\\d*\\\\\\(\\)");
        lexemes.forEach(System.out::println);
    }
}
